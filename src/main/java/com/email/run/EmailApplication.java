package com.email.run;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.email.service.EmailService;

@SpringBootApplication
@ComponentScan("com")
public class EmailApplication implements CommandLineRunner{

	@Autowired
	EmailService emailService;
	
	public static void main(String[] args) {
		SpringApplication.run(EmailApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
//	emailService.sendSimpleMessage("sujithdc@yahoo.com", "test email", "hi, Sujith is the Best!!!");	
	
	emailService.sendMessageWithAttachment("sujithdc@yahoo.com", "test email with attachment", "hi, Sujith is the Best!!!","d:/one_identifier.txt");	
		
	}

}
